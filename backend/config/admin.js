module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '4f8e5f54e5c5895916c5f0609fdc508d'),
  },
});
