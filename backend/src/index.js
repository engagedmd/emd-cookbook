"use strict";

const data = require("./data/data.json");

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register(/*{ strapi }*/) {},

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  async bootstrap({ strapi }) {
    // checking to see if there are recipes and employees existing in the database
    let seedRecipes = await strapi.db.query("api::recipe.recipe").findMany();
    let seedEmployees = await strapi.db
      .query("api::employee.employee")
      .findMany();

    // if there are no existing recipes and no existing employees then the data from data from data/data.json will be seeded
    if (!seedRecipes && !seedEmployees) {
      data.employees.forEach(async (employee) => {
        const employeeEntry = await strapi.entityService.create(
          "api::employee.employee",
          {
            data: {
              name: employee.name,
              position: employee.position,
              publishedAt: "2022-02-11T22:46:10.206Z",
            },
          }
        );

        const recipe = employee.recipe;
        const recipeEntry = await strapi.entityService.create(
          "api::recipe.recipe",
          {
            data: {
              title: recipe.title,
              description: recipe.description,
              ingredients: recipe.ingredients,
              instructions: recipe.instructions,
              year: recipe.year,
              employee: employeeEntry.id,
              publishedAt: "2022-02-11T22:46:10.206Z",
            },
          }
        );
      });
    }
  },
};
