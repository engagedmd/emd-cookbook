describe('Cookbook website layout', () => {
  it('displays navbar', () => {
    cy.visit('/')
    cy.getBySel('navbar').within(() => {
      cy.get('.makeStyles-branding-4').should('exist')
      cy.contains('Home').should('exist')
      cy.contains('2021').should('exist')
      cy.contains('2020').should('exist')
    })
  })
})
