describe('renders the homepage', () => {
  it('successfully loads', () => {
    cy.visit('/')
    cy.url().should('include', '/')
    cy.getBySel('homepage').should('exist')
    cy.get('img').should('be.visible')
    cy.getBySel('emd-intro').should('exist')
  })
})
