const apiRecipes = `${Cypress.env('apiUrl')}`

describe('recipe api', () => {
  context('get recipes', () => {
    it('gets a list of recipes', () => {
      cy.request('GET', `${apiRecipes}/api/recipes`).then((res) => {
        expect(res.status).to.eq(200)
        expect(res.body.data).length.to.be.greaterThan(1)
      })
    })
  })

  context('get single recipe', () => {
    it('gets a recipe by id', () => {
      cy.request('GET', `${apiRecipes}/api/recipes/1`).then((res) => {
        expect(res.status).to.eq(200)
        expect(res.body.data.attributes).to.have.any.keys(
          'title',
          'description',
          'ingredients',
          'instructions',
          'year'
        )
      })
    })
  })
})
