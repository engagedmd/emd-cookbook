const year = 2021

beforeEach(() => {
  cy.visit(`/recipe/${year}`)
})

describe('Displays a year recipe page and a list of recipe cards', () => {
  it('displays a single recipe content', () => {
    cy.url().should('include', `/recipe/${year}`)
    cy.getBySel('recipe-grid')
      .should('be.visible')
      .within(() => {
        cy.getBySel('recipe-card')
          .should('exist')
          .within(() => {
            cy.getBySel('recipe-card-img').should('be.visible')
            cy.getBySel('recipe-card-title').should('be.visible')
            cy.getBySel('recipe-card-button').should('be.visible')
          })
      })
  })

  it.only('can click on a recipe card button and see single recipe detail page', () => {
    cy.getBySel('recipe-card')
      .first()
      .within(() => {
        cy.getBySel('recipe-card-button').click()
      })
    cy.getBySel('recipe-title').should('be.visible')
    cy.getBySel('recipe-description').should('be.visible')
    cy.getBySel('recipe-ing').should('be.visible')
    cy.getBySel('recipe-steps').should('be.visible')
  })
})
