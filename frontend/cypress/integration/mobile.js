describe('Nav menu for mobile view', () => {
  it('clicks on menu button and displays side drawer', () => {
    cy.visit('/')
    cy.viewport('iphone-6')
    cy.getBySel('mobile-menu-button').should('be.visible').click()
    cy.getBySel('mobile-side-drawer').within(() => {
      cy.getBySel('mobile-links').should('exist')
    })
  })
})
