import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import WaveDesign from '../src/components/WaveDesign'

// import Image from 'next/image'

export interface IEmployee {
  data: {
    id: number
    attributes: {
      name: string
      position: string
      employeeImg: IEmployeeImg
    }
  }
}

export interface IEmployeeImg {
  data: {
    id: number
    attributes: {
      url: string
      formats: {
        thumbnail: {
          url: string
        }
      }
    }
  }
}

export interface IRecipeImg {
  data: {
    id: number
    attributes: {
      url: string
    }
  }
}

export interface IRecipe {
  id: number
  attributes: {
    title: string
    description: string
    ingredients: string
    instructions: string
    year?: string
    recipeImg: IRecipeImg
    employee: IEmployee
  }
}

const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
    paddingTop: '20px',
    marginTop: '75px',
  },
  hero: {
    display: 'flex',
  },
  heroImg: {
    objectFit: 'cover',
    width: '100%',
    height: '100%',
  },
  description: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    background: theme.palette.info.main,
    padding: '10%',
    paddingBottom: '0px',
    '& span': {
      color: theme.palette.secondary.main,
    },
  },
}))

export default function Home() {
  const classes = useStyles()

  return (
    <Container className={classes.root} data-testid="homepage">
      <div className={classes.hero}>
        <img src="/images/cookbook-hero.png" className={classes.heroImg} alt="holiday-banner" />
      </div>
      <div className={classes.description} data-testid="emd-intro">
        <Typography variant="h3">
          The EngagedMD team's holiday cookbook has become a beloved annual creation!
          <p>
            We love swapping recipes new and old with our teammates and sharing them with our
            community. It's so much fun to see what others call traditional and delicious, and to
            try them ourselves at home.
          </p>
          <p>
            Most of all, we love cozying up to enjoy good food and drink with our loved ones around
            the holidays. We hope you'll do the same.
          </p>
          <p>Happy holidays from our family to yours,</p>
          <p>
            The Engaged<span>MD</span> Team
          </p>
        </Typography>
      </div>
      <WaveDesign rotate="" marginTop="0px" background="rgba(233, 247, 247, 1)" />
    </Container>
  )
}
