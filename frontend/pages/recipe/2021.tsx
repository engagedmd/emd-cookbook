import React from 'react'
import { GetStaticProps } from 'next'
import axios from 'axios'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

import RecipeList from '../../src/components/RecipeList'
import ButtonLink from '../../src/components/ButtonLink'
import WaveDesign from '../../src/components/WaveDesign'
import { IRecipe } from '../index'

interface IPageProps {
  recipes: IRecipe[]
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    color: theme.palette.primary.main,
  },
  description: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    background: theme.palette.background.default,
    padding: '0 30px 30px 30px',
    '& span': {
      color: theme.palette.secondary.main,
    },
    [theme.breakpoints.down('sm')]: {
      height: '100%',
      marginTop: '30px',
      textAlign: 'left',
    },
  },
}))

export default function Recipes2021({ recipes }: IPageProps) {
  const classes = useStyles()
  return (
    <Container className={classes.root}>
      <WaveDesign rotate="rotate(180deg)" marginTop="90px" background="" />
      <div className={classes.description}>
        <Typography variant="h3">
          <p>We've put together this cookbook to share some of our little joys with you.</p>
          <p>We hope you enjoy these with your loved ones.</p>
          <p>Happy holidays from our family to yours,</p>
          <p>
            The Engaged<span>MD</span> Team
          </p>
        </Typography>
      </div>
      <RecipeList recipes={recipes} />
      <ButtonLink path="/" />
    </Container>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const res = await axios.get(
    'http://localhost:1337/api/recipes?filters[year][$eq]=2021&sort[0]=title%3Aasc&populate[0]=recipeImg&populate[1]=employee&populate[2]=employee.employeeImg'
  )
  const recipes = res.data.data
  return {
    props: {
      recipes,
    },
  }
}
