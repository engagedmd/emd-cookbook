import React from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import { GetStaticPaths, GetStaticProps } from 'next'
import ReactMarkdown from 'react-markdown'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import clsx from 'clsx'
import WaveDesign from '../../src/components/WaveDesign'

import { IRecipe } from '../index'

interface IRecipePageProps {
  recipe: IRecipe
}
const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
    display: 'flex',
    flexDirection: 'column',
    marginTop: '15px',
    [theme.breakpoints.down('xs')]: {
      paddingRight: '0',
      paddingLeft: '0',
    },
  },
  descriptionWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    witdth: '100%',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  recipeImage: {
    objectFit: 'cover',
    width: '500px',
    height: '500px',
  },
  description: {
    width: '50%',
    height: '100%',
    margin: '40px',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  descriptionDetail: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '40px',
  },
  title: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      fontSize: '32px',
      marginBottom: '30px',
    },
  },
  employeeImg: {
    borderRadius: '50%',
    objectFit: 'cover',
    width: '250px',
    height: '250px',
  },
  recipeDetail: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '100%',
    height: '100%',
    background: theme.palette.info.main,
    padding: '30px 100px',
    '& ol li': {
      marginBottom: '10px',
      borderBottom: `1px solid ${theme.palette.grey[400]}`,
    },
    '& ul': {
      listStyle: 'none',
    },
    [theme.breakpoints.down('sm')]: {
      height: '100%',
      padding: '30px 30px',
      alignItems: 'center',
    },
  },
  stepsBackground: {
    background: theme.palette.background.default,
  },
  buttonWrapper: {
    margin: '15px 0',
    [theme.breakpoints.down('xs')]: {
      margin: '15px',
    },
  },
}))

export default function Recipe({ recipe }: IRecipePageProps) {
  const router = useRouter()
  const { title, description, recipeImg, employee, ingredients, instructions } = recipe.attributes
  const { name, position, employeeImg } = employee.data.attributes
  const recipeImgPath = `http://localhost:1337${recipeImg.data?.attributes.url}`
  const employeeImgPath = `http://localhost:1337${employeeImg.data?.attributes.url}`
  const classes = useStyles()
  return (
    <Container className={classes.root}>
      <WaveDesign rotate="rotate(180deg)" marginTop="90px" background="" />
      <Typography variant="h1" className={classes.title} data-testid="recipe-title">
        {title}
      </Typography>
      <div className={classes.descriptionWrapper} data-testid="recipe-description">
        <img src={recipeImgPath} className={classes.recipeImage} alt={title} />
        <div className={classes.description}>
          <div className={classes.descriptionDetail}>
            <Typography variant="h3">{description}</Typography>
            <h4>
              {name} | {position}
            </h4>
            <img src={employeeImgPath} className={classes.employeeImg} alt={name} />
          </div>
        </div>
      </div>
      <div className={classes.recipeDetail} data-testid="recipe-ing">
        <Typography variant="h1">Ingredients</Typography>
        <ReactMarkdown>{ingredients}</ReactMarkdown>
      </div>
      <div
        className={clsx(classes.recipeDetail, classes.stepsBackground)}
        data-testid="recipe-steps"
      >
        <Typography variant="h1">Instructions</Typography>
        <ReactMarkdown>{instructions}</ReactMarkdown>
      </div>
      <div className={classes.buttonWrapper}>
        <Button
          size="small"
          color="primary"
          variant="contained"
          data-testid="back-button"
          onClick={() => router.back()}
        >
          Back to Recipes
        </Button>
      </div>
    </Container>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  // passing context allows to get the id that is in the url
  const res = await axios.get(
    `http://localhost:1337/api/recipes/${context.params.id}?populate[0]=recipeImg&populate[1]=employee&populate[2]=employee.employeeImg`
  )
  const recipe = res.data.data
  return {
    props: {
      recipe,
    },
  }
}

// if a page has dynamic routes and uses getStaticProps, it needs to define a list of paths to be statically generated.
// the paths key determines which paths will be pre-rendered
export const getStaticPaths: GetStaticPaths = async () => {
  const res = await axios.get('http://localhost:1337/api/recipes')
  const recipes = res.data.data
  const ids = recipes.map((recipe) => recipe.id)
  const paths = ids.map((id) => ({ params: { id: id.toString() } }))
  return {
    paths,
    fallback: false,
  }
}
