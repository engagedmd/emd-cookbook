import React from 'react'
import Link from 'next/link'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import CardHeader from '@material-ui/core/CardHeader'
import Avatar from '@material-ui/core/Avatar'

import { makeStyles } from '@material-ui/core/styles'

import { IRecipe } from '../../pages/index'

interface IRecipeProps {
  recipe: IRecipe
}

const useStyles = makeStyles(({ palette }) => ({
  root: {
    maxWidth: 345,
    background: palette.info.main,
  },
  media: {
    height: 250,
  },
  cardContent: {
    height: '75px',
    color: palette.primary.main,
  },
  avatar: {
    color: palette.primary.main,
  },
}))

function RecipeCard({ recipe }: IRecipeProps) {
  const classes = useStyles()
  const { title, recipeImg, employee } = recipe.attributes

  const imagePath = `http://localhost:1337${recipeImg.data?.attributes.url}`
  const thumbnail = employee.data.attributes.employeeImg.data?.attributes.formats.thumbnail.url

  return (
    <Card className={classes.root} data-testid="recipe-card">
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={imagePath}
          title={title}
          data-testid="recipe-card-img"
        />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h4" component="h2" data-testid="recipe-card-title">
            {title}
          </Typography>
        </CardContent>
        <CardHeader
          className={classes.avatar}
          avatar={
            <Avatar
              aria-label="recipe"
              alt={employee.data.attributes.name}
              src={`http://localhost:1337${thumbnail}`}
            />
          }
          title={`by ${employee.data.attributes.name}`}
        />
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" data-testid="recipe-card-button">
          <Link href={`/recipe/${recipe.id}`}>Go to Recipe</Link>
        </Button>
      </CardActions>
    </Card>
  )
}
export default RecipeCard
