import React from 'react'
import Grid from '@material-ui/core/Grid'
import SingleRecipe from './SingleRecipe'

import { IRecipe } from '../../pages/index'

export interface IRecipeListProps {
  recipes: IRecipe[]
}

function RecipeList({ recipes }: IRecipeListProps) {
  return (
    <Grid container spacing={2} data-testid="recipe-grid">
      {recipes.map((recipe) => (
        <Grid item key={recipe.id} xs={12} sm={6} md={3}>
          <SingleRecipe recipe={recipe} />
        </Grid>
      ))}
    </Grid>
  )
}

export default RecipeList
