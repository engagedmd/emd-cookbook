import React, { useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Avatar from '@material-ui/core/Avatar'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    boxShadow: 'none',
    width: '100%',
    height: '78px',
    color: theme.palette.primary.main,
    padding: '15px 60px',
    [theme.breakpoints.down('sm')]: {
      padding: '15px 5px',
    },
  },
  link: {
    marginRight: '20px',
    cursor: 'pointer',
  },
  logo: {
    background: 'white',
    marginRight: '20px',
    width: theme.spacing(7),
    height: theme.spacing(7),
    border: theme.palette.secondary.main,
  },
  branding: {
    marginRight: 'auto',
    display: 'flex',
    alignItems: 'center',
    '& span': {
      color: theme.palette.secondary.main,
    },
  },
  menuButton: {
    color: theme.palette.primary.main,
  },
  active: {
    borderBottom: `2px solid ${theme.palette.secondary.main}`,
  },
}))

export default function Navbar() {
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const router = useRouter()

  return (
    <AppBar className={classes.root} data-testid="navbar">
      <Container>
        <Toolbar disableGutters>
          <Link href="/" passHref>
            <div className={classes.branding}>
              <Avatar className={classes.logo} src="/images/engagedmd-logo.png">
                EMD
              </Avatar>
              <Typography variant="h3">
                Engaged<span>MD</span>
              </Typography>
            </div>
          </Link>
          <Hidden xsDown>
            <Link href="/" passHref>
              <Typography
                variant="h3"
                className={clsx(router.pathname === '/' ? `${classes.active}` : '', classes.link)}
              >
                Home
              </Typography>
            </Link>
            <Link href="/recipe/2021" passHref>
              <Typography
                variant="h3"
                className={clsx(
                  router.pathname === '/recipe/2021' ? `${classes.active}` : '',
                  classes.link
                )}
              >
                2021
              </Typography>
            </Link>
            <Link href="/recipe/2020" passHref>
              <Typography
                variant="h3"
                className={clsx(
                  router.pathname === '/recipe/2020' ? `${classes.active}` : '',
                  classes.link
                )}
              >
                2020
              </Typography>
            </Link>
          </Hidden>
          <Hidden smUp>
            <IconButton aria-label="menu" data-testid="mobile-menu-button">
              <MenuIcon className={classes.menuButton} onClick={() => setOpen(true)} />
            </IconButton>
          </Hidden>
        </Toolbar>
      </Container>
      <SwipeableDrawer
        anchor="right"
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => setOpen(false)}
        data-testid="mobile-side-drawer"
      >
        <div>
          <IconButton>
            <ChevronRightIcon onClick={() => setOpen(false)} />
          </IconButton>
        </div>
        <Divider />
        <List data-testid="mobile-links">
          <Link href="/" passHref>
            <ListItem>Home</ListItem>
          </Link>
          <Link href="/recipe/2021" passHref>
            <ListItem>2021</ListItem>
          </Link>
          <Link href="/recipe/2020" passHref>
            <ListItem>2020</ListItem>
          </Link>
        </List>
      </SwipeableDrawer>
    </AppBar>
  )
}
