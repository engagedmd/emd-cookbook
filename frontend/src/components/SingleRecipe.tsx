import React from 'react'
import RecipeCard from './RecipeCard'

import { IRecipe } from '../../pages/index'

interface IRecipeProps {
  recipe: IRecipe
}

export function Recipe({ recipe }: IRecipeProps) {
  return <RecipeCard recipe={recipe} />
}

export default Recipe
