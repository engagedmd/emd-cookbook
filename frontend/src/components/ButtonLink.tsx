import React from 'react'
import Link from 'next/link'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  buttonWrapper: {
    margin: '15px 0',
  },
})

export default function ButtonLink({ path }) {
  const classes = useStyles()
  return (
    <div className={classes.buttonWrapper}>
      <Link href={path} passHref>
        <Button size="small" variant="contained" color="primary" data-testid="back-button">
          Back to home
        </Button>
      </Link>
    </div>
  )
}
