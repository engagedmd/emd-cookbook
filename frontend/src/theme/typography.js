const typography = {
  fontFamily: 'Rubik',
  fontSize: 16,
  fontWeightMedium: 400,
  h1: {
    fontFamily: 'Rubik',
    fontWieght: 600,
    fontSize: 45,
    letterSpacing: -0.5,
  },
  h2: {
    fontFamily: 'Rubik',
    fontWieght: 400,
    fontSize: 32,
    letterSpacing: -0.5,
  },
  h3: {
    fontFamily: 'Rubik',
    fontWieght: 400,
    fontSize: 24,
    letterSpacing: -0.5,
    lineHeight: 1.5,
  },
  h4: {
    fontFamily: 'Rubik',
    fontWeight: 500,
    fontSize: 20,
    letterSpacing: -0.5,
  },
  h5: {
    fontFamily: 'Rubik',
    fontWeight: 400,
    fontSize: 16,
    letterSpacing: 1,
  },
  h6: {
    fontFamily: 'Rubik',
    fontWeight: 500,
    fontSize: 14,
    letterSpacing: 0,
  },
}

export default typography
