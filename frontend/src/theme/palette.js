const palette = {
  primary: {
    main: '#013B58',
  },
  secondary: {
    main: '#22AEB0',
  },
  warning: {
    main: '#F4D25A',
  },
  info: {
    main: '#E9F7F7',
  },
  background: {
    default: 'white',
  },
}

export default palette
